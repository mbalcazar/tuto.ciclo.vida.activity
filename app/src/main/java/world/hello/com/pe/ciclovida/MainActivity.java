package world.hello.com.pe.ciclovida;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(MainActivity.this,"Activity Created", Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onStart() {
        // TODO Auto-generated method stub

        Toast.makeText(MainActivity.this,"Start Activity",Toast.LENGTH_LONG).show();
        super.onStart();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Toast.makeText(MainActivity.this,"Resume Activity",Toast.LENGTH_LONG).show();
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Toast.makeText(MainActivity.this,"Pause the Activity ",Toast.LENGTH_LONG).show();
        super.onPause();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Toast.makeText(MainActivity.this,"Stop Activity",Toast.LENGTH_LONG).show();
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Toast.makeText(MainActivity.this,"Restart Activity",Toast.LENGTH_LONG).show();
        // TODO Auto-generated method stub
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Toast.makeText(MainActivity.this,"Destroy Activity",Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

}

